extends Node

#@onready var hud = $CanvasLayer/HUD
#@onready var health_bar = $CanvasLayer/HUD/HealthBar
const escape_menu_path = "res://ui/escape_menu.tscn" as String

@export var fps_counter_path: NodePath
@onready var fps_counter: Label = get_node(fps_counter_path)


func _unhandled_input(event):
	if Input.is_action_just_pressed("quit"):
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		GameEvents.menu_option_selected.emit(escape_menu_path)



func _process(delta):
	# Godot provides the current FPS in the Engine singleton
	var fps = Engine.get_frames_per_second()
	
	# Update the FPS label
	fps_counter.text = str("FPS: ", fps)

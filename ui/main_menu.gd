extends CanvasLayer

const game_mode_path = "res://ui/game_mode_menu.tscn" as String
const settings_path = "res://ui/settings_menu.tscn" as String


func _ready():
	%PlayButton.pressed.connect(on_PlayButton_pressed)
	%SettingsButton.pressed.connect(on_SettingsButton_pressed)
	%QuitProgramButton.pressed.connect(on_QuitProgramButton_pressed)


func on_PlayButton_pressed():
	GameEvents.emit_menu_option_selected(game_mode_path)


func on_SettingsButton_pressed():
	GameEvents.emit_menu_option_selected(settings_path)


func on_QuitProgramButton_pressed():
	get_tree().quit()

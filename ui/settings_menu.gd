extends CanvasLayer

@onready var window_button: Button = $%WindowButton
@onready var back_button = %BackButton


# Called when the node enters the scene tree for the first time.
func _ready():
	back_button.pressed.connect(on_BackButton_pressed)
	window_button.pressed.connect(on_WindowButton_pressed)

	update_display()


func update_display():
	window_button.text = "Windowed"
	if DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_FULLSCREEN:
		window_button.text = "Fullscreen"


func on_BackButton_pressed():
	GameEvents.emit_menu_back_selected()


func on_WindowButton_pressed():
	var mode = DisplayServer.window_get_mode()
	if mode != DisplayServer.WINDOW_MODE_FULLSCREEN:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	else:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		
	update_display()

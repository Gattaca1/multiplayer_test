extends Node

var ui_stack: Array = [] # This should be an array of node references
const main_menu_path = "res://ui/main_menu.tscn" as String

func _ready():
	GameEvents.menu_option_selected.connect(open_menu)
	GameEvents.menu_back_selected.connect(close_current_menu)
	GameEvents.menu_clear_all.connect(close_all_menus)
	# Start with the main menu
	open_menu(main_menu_path)

func open_menu(ui_path: String):
	# Load the new menu
	var new_ui_scene = load(ui_path) as PackedScene
	if ( new_ui_scene == null ): return
	
	var new_ui_scene_instance = new_ui_scene.instantiate()
	if ( new_ui_scene_instance == null ): return
	
	# Add the menu to the screen
	get_tree().current_scene.add_child.call_deferred(new_ui_scene_instance)

	# Hide the current (top) UI if it exists
	if ui_stack.size() > 0:
		ui_stack[-1].hide()
	  
	# Push the new UI onto the stack and make it visible
	ui_stack.append(new_ui_scene_instance)

func close_current_menu():
	# Close the current (top) UI
	if ui_stack.size() > 0:
		var current_ui = ui_stack.pop_back()
		current_ui.hide()
		current_ui.queue_free()  # Use queue_free() if the instance won't be reused
		
		# Ensure the previous UI is now visible
		ui_stack[-1].show()


func close_all_menus():
	while ui_stack.size() > 0:
		var current_ui = ui_stack.pop_back()
		current_ui.queue_free()

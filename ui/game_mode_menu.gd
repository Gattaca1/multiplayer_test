extends CanvasLayer

@onready var address_entry: LineEdit = %AddressEntry
@onready var back_button: Button = %BackButton
@onready var host_button: Button = %HostButton
@onready var join_button: Button = %JoinButton

# Called when the node enters the scene tree for the first time.
func _ready():
	back_button.pressed.connect(on_BackButton_pressed)
	host_button.pressed.connect(on_HostButton_pressed)
	join_button.pressed.connect(on_JoinButton_pressed)


func on_BackButton_pressed():
	GameEvents.emit_menu_back_selected()


func on_HostButton_pressed():
	GameEvents.emit_menu_clear_all()
	NetworkEvents.emit_host_game_selected()


func on_JoinButton_pressed():
	GameEvents.emit_menu_clear_all()
	NetworkEvents.emit_join_game_selected(address_entry.text)

extends CanvasLayer

@onready var resume_button: Button = %ResumeButton
@onready var settings_button: Button = %SettingsButton
@onready var quit_match_button: Button = %QuitMatchButton

const settings_path = "res://ui/settings_menu.tscn" as String


func _ready():
	resume_button.pressed.connect(on_ResumeButton_pressed)
	settings_button.pressed.connect(on_SettingsButton_pressed)
	quit_match_button.pressed.connect(on_QuitMatchButton_pressed)


func _unhandled_input(event):
	if Input.is_action_just_pressed("tab"):
		on_ResumeButton_pressed()


func on_ResumeButton_pressed():
	GameEvents.menu_clear_all.emit()
	
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func on_SettingsButton_pressed():
	GameEvents.menu_option_selected.emit(settings_path)


func on_QuitMatchButton_pressed():
	get_tree().quit()

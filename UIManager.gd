#extends CanvasLayer
extends Node
# Code to update the health, ammo, and leaderboard with networking data
"""
onready var health_label = $HealthLabel
onready var ammo_label = $AmmoLabel
onready var leaderboard_panel = $Leaderboard

# These functions would be connected to signals emitted by the player or GameManager
func _on_Player_health_changed(new_health):
	health_label.text = str(new_health)

func update_health(new_health):
	# Update the health display based on the "new_health" value
	pass
	
func _on_Weapon_ammo_changed(new_ammo):
	ammo_label.text = str(new_ammo)

func _on_GameManager_leaderboard_updated(updated_leaderboard):
	update_leaderboard(updated_leaderboard)

func update_leaderboard(leaderboard_data):
	# Clear existing leaderboard
	leaderboard_panel.get_child(0).queue_free()

	# Add new leaderboard entries
	for data in leaderboard_data:
		var new_entry = LeaderboardEntry.instance() # Assuming a pre-made scene for entries
		new_entry.set_player_data(data)
		leaderboard_panel.add_child(new_entry)
"""

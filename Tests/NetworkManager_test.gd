extends "res://addons/gut/test.gd"
"""
func before_each():
	# Resets the network state before each test.
	get_tree().network_peer = null

func test_can_create_server():
	NetworkManager.create_server()
	assert_is_not_null(get_tree().network_peer)
	assert_true(get_tree().network_peer is NetworkedMultiplayerENet)

func test_can_join_server():
	NetworkManager.join_server("127.0.0.1")
	assert_not_eq(get_tree().network_peer.get_connection_status(), NetworkedMultiplayerPeer.CONNECTION_DISCONNECTED)

func after_each():
	# Clean up network peer after each test.
	get_tree().network_peer.close_connection()
"""

extends "res://addons/gut/test.gd"

var game_manager = null
""""
func before_each():
	game_manager = preload("res://GameManager.gd")

func test_game_starts_with_empty_board():
	var expected_board = [["", "", ""], ["", "", ""], ["", "", ""]]
	assert_eq(game_manager.game_board, expected_board)

func test_place_on_board_places_symbol():
	game_manager.place_on_board(0, "X")
	assert_eq(game_manager.game_board[0], "X")

func test_process_kill_swaps_turns():
	game_manager.process_kill("player1", "player2", 0)
	assert_eq(game_manager.current_turn, "O")
"""

extends "res://addons/gut/test.gd"

var player = null
"""
func before_each():
	player = preload("res://scenes/Player.tscn").instance()

func test_player_respawn_resets_health():
	player.health = 0
	player.respawn()
	assert_eq(player.health, 3)

func test_player_shoot_decreases_ammo():
	var initial_ammo = player.get_gun_ammo()
	player.shoot()
	assert_eq(player.get_gun_ammo(), initial_ammo - 1)
"""

class_name NetworkManager
extends Node

signal player_connected(player_id)
signal player_disconnected(player_id)

var enet_peer = ENetMultiplayerPeer.new()
const ip = "localhost" as String

const Player = preload("res://player.tscn")
const PORT = 9999
const MAX_CLIENTS = 16 as int

func _ready():
	NetworkEvents.host_game_selected.connect(_on_host_button_pressed)
	NetworkEvents.join_game_selected.connect(_on_join_button_pressed)


func _on_host_button_pressed():	
	# Start as server
	enet_peer.create_server(PORT, MAX_CLIENTS)
	if enet_peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer server.")
		return
	multiplayer.multiplayer_peer = enet_peer
	
	#Connect the signals
	multiplayer.peer_connected.connect(add_player)
	multiplayer.peer_disconnected.connect(remove_player)
	
	add_player(multiplayer.get_unique_id())
	
	upnp_setup()


func _on_join_button_pressed(ip_address: String):
	# Start as client
	if ( ip_address == "" ):
		OS.alert("Need a remote to connect to.")
		return
	
	enet_peer.create_client(ip_address, PORT)
	
	if enet_peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer client.")
		return
	
	multiplayer.multiplayer_peer = enet_peer


func add_player(peer_id):
	var player = Player.instantiate()
	player.name = str(peer_id)
	call_deferred("add_child", player)
	#if player.is_multiplayer_authority():
	#	player.health_changed.connect(update_health_bar)


func remove_player(peer_id):
	var player = get_node_or_null(str(peer_id))
	if player:
		player.queue_free()



"""
func update_health_bar(health_value):
	health_bar.value = health_value
"""

func _on_multiplayer_spawner_spawned(node):
	if node.is_multiplayer_authority():
		#node.health_changed.connect(update_health_bar)
		pass


func upnp_setup():
	var upnp = UPNP.new()
	
	var discover_result = upnp.discover()
	
	assert(discover_result == UPNP.UPNP_RESULT_SUCCESS, \
		"UPNP Discover Failed! Error %s" % discover_result)
	
	assert(upnp.get_gateway() and upnp.get_gateway().is_valid_gateway(), \
		"UPNP Invalid Gateway!")
	
	var map_result = upnp.add_port_mapping(PORT)
	assert(map_result == UPNP.UPNP_RESULT_SUCCESS, \
		"UPNP Port Mapping Failed! Error %s" % map_result)
	
	print("Success! Join Address: %s" % upnp.query_external_address())

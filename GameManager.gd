class_name GameManager
extends Node

var current_turn = "X" # Or "O"
var game_board = [["", "", ""], ["", "", ""], ["", "", ""]]

func _ready():
	# Initialize the game state
	pass

func process_kill(killer_id, victim_id, quadrant):
	# Code to update the game state based on player actions
	if ( can_place( quadrant ) ):
		place_on_board(quadrant, current_turn)
		swap_turns()
		rpc("update_game_board", game_board)

@rpc("call_local") # Makes this function sync in multiplayer. calling on player we have authority over
func update_game_board(new_board):
	game_board = new_board
	# Update UI and game board visuals accordingly

func can_place(quadrant):
	pass


func place_on_board(quadrant, symbol):
	game_board[quadrant] = symbol

func swap_turns():
	if current_turn == "X":
		current_turn = "O"
	else:
		current_turn = "X"

"""
# Script attached to each quadrant of the Map.
extends Area

func _on_player_killed_within_quadrant(killer_id):
	if killer_id == get_tree().get_network_unique_id():
		var quadrant_position = get_quadrant_position_from_global_position(global_transform)
		get_node("/root/GameManager").attempt_claim_quadrant(killer_id, quadrant_position)
"""

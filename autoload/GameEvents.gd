extends Node

signal menu_option_selected(scene_path: String)
signal menu_back_selected()
signal menu_clear_all()


func emit_menu_option_selected(scene_path: String):
	menu_option_selected.emit(scene_path)


func emit_menu_back_selected():
	menu_back_selected.emit()


func emit_menu_clear_all():
	menu_clear_all.emit()

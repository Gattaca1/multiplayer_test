extends Node

signal join_game_selected(ip_address: String)
signal host_game_selected()

func emit_join_game_selected(ip_address: String):
	join_game_selected.emit(ip_address)


func emit_host_game_selected():
	host_game_selected.emit()
